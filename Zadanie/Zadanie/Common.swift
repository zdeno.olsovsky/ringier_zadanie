//
//  Common.swift
//  Zadanie
//
//  Created by Zdenko Olsovsky on 16/02/2020.
//  Copyright © 2020 Zdenko Olsovsky. All rights reserved.
//

import Foundation
import UIKit

class Common {
    
    static let shared = Common()
    
    static func setAlert(text:String)-> UIAlertController{
        let alert = UIAlertController(title: nil, message: text, preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        return alert
    }
}
