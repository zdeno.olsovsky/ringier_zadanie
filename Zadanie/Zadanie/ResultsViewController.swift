import Foundation
import UIKit

extension UIImageView {
    func loadImageAsync(urlString: String) {
        self.image = nil
        let url = URL(string: urlString)
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url!) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

class ResultsViewCell: UITableViewCell{
    @IBOutlet var imageContainer: UIImageView!
    @IBOutlet var imageLabel: UILabel!
}

class ResultsViewController: UITableViewController {
    
    @IBOutlet var mainTable: UITableView!
    
    var searchResults:Welcome?
    var detailUrl = ""
    var loadedMainImage = UIImage()
    var detailAuthor = ""
    
    override func viewDidLoad() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 300
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let results = searchResults?.results else {
            return 0
        }
        return results.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "pictureCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ResultsViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
                
        cell.imageContainer.loadImageAsync(urlString: (searchResults!.results?[indexPath.row].urls?.thumb)!)
        cell.imageLabel.text = searchResults!.results?[indexPath.row].altDescription
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Cell selected")
        self.detailUrl = (self.searchResults?.results![indexPath.row].urls?.regular)!
        self.detailAuthor = (self.searchResults?.results![indexPath.row].user?.name)!
        self.performSegue(withIdentifier: "showDetail", sender: self)
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let detailViewController = segue.destination as? DetailViewController
            else {
                return
        }
        detailViewController.mainImageUrl = self.detailUrl
        detailViewController.author = self.detailAuthor
    }
}
