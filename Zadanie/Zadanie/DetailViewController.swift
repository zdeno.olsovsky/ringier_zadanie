//
//  DetailViewController.swift
//  Zadanie
//
//  Created by Zdenko Olsovsky on 16/02/2020.
//  Copyright © 2020 Zdenko Olsovsky. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var firstLabel: UILabel!
    
    var mainImageUrl = ""
    var author = ""
    var activityIndicator : UIActivityIndicatorView?
    
    override func viewDidLoad() {
        self.firstLabel.text = "Author: \(self.author)"
        showActivityIndicatory()
        downloadMainImage()
    }
    
    func downloadMainImage(){
        let url = URL(string: self.mainImageUrl)

        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard let data = data, error == nil else { return }
            if let image = UIImage(data: data){
                DispatchQueue.main.async() {    // execute on main thread
                       self.activityIndicator?.stopAnimating()
                       self.mainImage.image = image
                }
            }
        }

        task.resume()
    }
    
     func showActivityIndicatory() {
        activityIndicator = UIActivityIndicatorView(style: .large)
        let activityIndicatorPosition = CGPoint(x: self.mainImage.bounds.size.width/2, y: self.mainImage.bounds.size.height/2)
        activityIndicator!.center = activityIndicatorPosition
        self.view.addSubview(activityIndicator!)
        activityIndicator!.startAnimating()
    }
}
