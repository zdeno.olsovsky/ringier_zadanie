//
//  RestLibrary.swift
//  Zadanie
//
//  Created by Zdenko Olsovsky on 10/02/2020.
//  Copyright © 2020 Zdenko Olsovsky. All rights reserved.
//

import Foundation

class RESTClient{
    
    enum restType {
        case GET
    }
    
    func fetchData(urlToFetch: String, completion: @escaping (Welcome?, Error?) -> Void) {
        let url = URL(string: urlToFetch)!

        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            print("Data")
            guard let data = data else { return }
            do {
                let decoder = JSONDecoder()
                let product = try decoder.decode(Welcome.self, from: data)
                print("Result obtained")
                completion(product, nil)
            } catch {
                print(error)
                print("Could not decode")
                completion(nil, error)
            }
        }
        task.resume()
    }
}
