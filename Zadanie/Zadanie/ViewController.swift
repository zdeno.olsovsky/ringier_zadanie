//
//  ViewController.swift
//  Zadanie
//
//  Created by Zdenko Olsovsky on 10/02/2020.
//  Copyright © 2020 Zdenko Olsovsky. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var data:Welcome?
    
    @IBOutlet var textFieldVar: UITextField!
    
    
    @IBAction func searchButton(_ sender: UIButton) {
        guard let text = textFieldVar.text, !text.isEmpty else {
            return
        }
        presentResults(searchTerm: text)
    }
    
    func presentResults(searchTerm: String) {
        let alert = Common.setAlert(text: "Searching...")
        present(alert, animated: true, completion: nil)
        RESTClient().fetchData(urlToFetch: "https://api.unsplash.com/search/photos?page=1&per_page=30&query=\(searchTerm.trimmingCharacters(in: .whitespacesAndNewlines))", completion: { data, error in
            if data != nil {
                print("Fetched data")
                self.data = data
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: {
                        self.performSegueWithData()
                    })
                }
            }
            
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func performSegueWithData() {
        self.performSegue(withIdentifier: "showResultSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let resultViewController = segue.destination as? ResultsViewController
            else {
                return
        }
        resultViewController.searchResults = self.data
    }
    
}

